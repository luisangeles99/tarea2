﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubo : MonoBehaviour
{
    [SerializeField]
    private float speed = 2;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W)) {
            Debug.Log("W presionado");
            transform.position += new Vector3(0, 1, 0) * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("S presionado");
            transform.position += new Vector3(0, -1, 0) * Time.deltaTime * speed;
        }
    }
}
